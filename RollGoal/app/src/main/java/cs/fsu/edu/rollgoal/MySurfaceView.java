package cs.fsu.edu.rollgoal;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.support.constraint.solver.Goal;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

public class MySurfaceView extends SurfaceView implements SurfaceHolder.Callback {



    SurfaceHolder holder;
    GameThread gameThread;
    Resources res;
    Rect rectLeftWall;
    Rect rectBottomWall;
    Rect rectTopWall;
    Rect rectRightWall;
    Rect wall1;
    Rect wall2;

    static float accelerationX;
    static float accelerationY;
    static float positionX;
    static float positionY;


    static boolean lose;
    static boolean goal;

    public MySurfaceView(Context context){
        super(context);
        accelerationX = 0;
        accelerationY = 0;
        positionX = 1000;
        positionY = 215;
        lose = false;
        goal = false;
        rectLeftWall = new Rect(0,0,100,900);
        rectBottomWall = new Rect(0, 800, 1800 ,900);
        rectRightWall = new Rect(1700,0,1800,900);
        rectTopWall = new Rect(0,0,1800, 100);

        wall1 = new Rect(950,0,1000,400);
        wall2 = new Rect(600,250,1200,500);

        res = getResources();
        holder = getHolder();
        holder.addCallback(this);

        setWillNotDraw(false);

        Log.i("MySurfaceView", "Created");

    }
    boolean goal(){

        // x = 839 +- 30
        //y = 143 +- 30
        if (MySurfaceView.positionX < 839 + 30 &&
                positionX > 839 -30 &&
                positionY < 143 + 30 &&
                positionY > 143 - 30){
            return true;
        }
        return false;
    }

    boolean gameOver(){
        //x1488
        //y199
        if (positionX < 1488 + 50 &&
                positionX > 1488 - 50 &&
                positionY < 199 + 50 &&
                positionY > 199 - 50){
            return true;
        }

        //x1356
        //y568
        if (positionX < 1356 + 50 &&
                positionX > 1356 - 50 &&
                positionY < 568 + 50 &&
                positionY > 568 - 50){
            return true;
        }
Rect rect = new Rect();
        rect.bottom++;
        //x1480
        //y716
        if (positionX < 1480 + 50 &&
                positionX > 1480 - 50 &&
                positionY < 716 + 50 &&
                positionY > 716 - 50){
            return true;
        }

        //x747
        //y584
        if (positionX < 747 + 50 &&
                positionX > 747 - 50 &&
                positionY < 584 + 50 &&
                positionY > 584 - 50){
            return true;
        }

        //x595
        //y732
        if (positionX < 595 + 50 &&
                positionX > 595 - 50 &&
                positionY < 732 + 50 &&
                positionY > 732 - 50){
            return true;
        }

        //x479
        //y608
        if (positionX < 479 + 50 &&
                positionX > 479 - 50 &&
                positionY < 608 + 50 &&
                positionY > 608 - 50){
            return true;
        }

        //x239
        //y560
        if (positionX < 239 + 50 &&
                positionX > 239 - 50 &&
                positionY < 560 + 50 &&
                positionY > 560 - 50){
            return true;
        }

        return false;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        if (gameThread != null)
            return;

        gameThread = new GameThread(getHolder());
        gameThread.start();

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        gameThread.running = false;
    }

    @Override
    protected void onDraw(Canvas canvas) {

    }

    private class GameThread extends Thread{

        public boolean running = true;
        private SurfaceHolder holder;

        public GameThread(SurfaceHolder surfaceHolder){
            holder = surfaceHolder;
        }


        //game loop
        public void run() {
            Log.i("MySurfaceView", "gameThread running");
            Bitmap bitmap = BitmapFactory.decodeResource(res, R.drawable.testcourse1);
            Paint p = new Paint();
            Canvas canvas = null;
            Rect screen = new Rect();
            screen.set(0,0,1800,890);


            //game loop
            while (running) {
                Log.i("MySurfaceView", String.valueOf(accelerationY));

                try {

                    positionX += accelerationX;
                    positionY += accelerationY;

                    //check collision
                    if (rectLeftWall.contains((int)positionX-50,(int)positionY)){
                        positionX = rectLeftWall.right + 50;
                    }
                    if (rectBottomWall.contains((int)positionX,(int)positionY+50)){
                        positionY = rectBottomWall.top - 50;
                    }
                    if (rectRightWall.contains((int)positionX+50,(int)positionY)){
                        positionX = rectRightWall.left - 50;
                    }

                    if (rectTopWall.contains((int)positionX,(int)positionY-50)){
                        positionY = rectTopWall.bottom + 50;
                    }

                    if (wall1.contains((int)positionX-50, (int)positionY)){
                        positionX = wall1.right + 50;
                    }

                    if (wall2.contains((int)positionX-50, (int)positionY)){
                        positionX = wall2.right + 50;
                    }
                    if (wall2.contains((int) positionX + 50, (int) positionY)){
                        positionX = wall2.left - 50;
                    }
                    if (wall2.contains((int)positionX, (int)positionY + 50)){
                        positionY = wall2.top - 50;
                    }
                    if (wall2.contains((int)positionX, (int)positionY - 50)){
                        positionY = wall2.bottom + 50;
                    }


                    canvas = holder.lockCanvas();

                    canvas.drawBitmap(bitmap,null,screen,p);
                    //draw bg
                    p.setColor(0xff888888);
                    //canvas.drawRect(screen, p);
                    //draw walls
                    p.setColor(0xff444444);

                    canvas.drawRect(rectLeftWall, p);
                    canvas.drawRect(rectBottomWall, p);
                    canvas.drawRect(rectRightWall, p);
                    canvas.drawRect(rectTopWall, p);
                    canvas.drawRect(wall1, p);
                    canvas.drawRect(wall2, p);
                    p.setColor(Color.WHITE);
                    canvas.drawCircle(positionX,positionY,50,p);
                    if (goal()){
                        bitmap = BitmapFactory.decodeResource(res, R.drawable.rollgoal_goal);
                        canvas.drawColor(Color.BLACK);
                        canvas.drawBitmap(bitmap,null,screen, p);
                        running = false;

                    }

                    else if (gameOver()){
                        bitmap = BitmapFactory.decodeResource(res, R.drawable.rollgoal_gameover);
                        canvas.drawColor(Color.BLACK);
                        canvas.drawBitmap(bitmap,null,screen, p);
                        running = false;

                    }


                    synchronized (holder){
                        onDraw(canvas);
                    }
                }
                finally {
                    if (canvas != null)
                        holder.unlockCanvasAndPost(canvas);
                }
            }
        }
    }
}

