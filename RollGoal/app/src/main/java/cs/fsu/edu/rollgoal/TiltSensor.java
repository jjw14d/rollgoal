package cs.fsu.edu.rollgoal;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public class TiltSensor implements SensorEventListener {
    private SensorManager manager;
    private Sensor sensor;



    public TiltSensor(Context context){
        manager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
        sensor = manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (sensor == null)
            Log.i("TiltSensor", "Unsupported Device");
        manager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float[] values = event.values;

        MySurfaceView.accelerationY = values[0];
        MySurfaceView.accelerationX = values[1];


    }
}
